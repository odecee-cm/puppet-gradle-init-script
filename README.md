# Gradle Init Script Puppet Module

This module is for installing a gradle init script on a bambooagent.
This script contains the following:
1. a repository setting
2. a function for publishing artifacts to nexus

## To use it in a consuming repo simply follow the following steps:

### Add the following lines to your Puppetfile:
    mod 'puppet-gradle-init-script', :git => 'git@bitbucket.org:odecee-cm/puppet-gradle-init-script.git'

### Retrieve the relevant repos by using the command:
    r10k puppetfile install
