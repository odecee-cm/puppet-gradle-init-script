class puppet-gradle-init-script::install {
  
  file { "${bambooelasticagent::params::user_home}/.gradle/init.d":
    ensure => directory,
    group  => $bambooelasticagent::params::group,
    owner  => $bambooelasticagent::params::user,
    mode   => '0755',
  }

  file { "${bambooelasticagent::params::user_home}/.gradle/init.d/odecee_common.gradle":
    ensure  => file,
    require => [File["${bambooelasticagent::params::user_home}/.gradle/init.d"], Class['gradle::install']],
    content => template('puppet-gradle-init-script/odecee_common.gradle.erb'),
    group   => $bambooelasticagent::params::group,
    owner   => $bambooelasticagent::params::user,
    mode    => '0644',
  }
}