class puppet-gradle-init-script {
    require gradle
    require bambooelasticagent
    include puppet-gradle-init-script::params
    include puppet-gradle-init-script::install
}