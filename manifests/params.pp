class puppet-gradle-init-script::params {

# Nexus params
$nexus_read_only_user        = hiera('nexus_read_only_user')
$nexus_read_only_password    = hiera('nexus_read_only_password')
$nexus_elb                   = hiera('nexus_elb')
$nexus_http_port             = hiera('nexus_http_port')
$nexus_thirdparty_repository = hiera('nexus_thirdparty_repository')
$nexus_deployment_user       = hiera('nexus_deployment_user')
$nexus_deployment_password   = hiera('nexus_deployment_password')
}